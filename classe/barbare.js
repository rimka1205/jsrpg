const Combattant = require("./combattant");

class Barbare extends Combattant {
    constructor() {
        super();
        super.pv += 10;
    }
}

module.exports = Barbare;
