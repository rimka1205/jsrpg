const Joueur = require("./joueur");

class Magicien extends Joueur {
    constructor() {
        super();
        super.intelligence += 5;
    }

    attack(name) {
        if (name == "normal") {
            return super.intelligence;
        }
    }
}

module.exports = Magicien
