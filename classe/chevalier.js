const Combattant = require("./combattant");

class Chevalier extends Combattant {
    constructor() {
        super();
        super.pv += 5;
        super.strength += 5;
    }
}

module.exports = Chevalier;
