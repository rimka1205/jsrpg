const Magicien = require("./magicien");

class Elementaire extends Magicien {
    element
    constructor() {
        super();
        let tableElement = ['eau','feu','air','terre'];
        let random = Math.floor(Math.random() * tableElement.length);
        this.element = tableElement[random];
    }

    get element() {
        return this.element;
    }
}

module.exports = Elementaire;
