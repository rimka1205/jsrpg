const Magicien = require("./magicien");

class Enchanteur extends Magicien {
    enchantemant;
    constructor() {
        super();
        this.enchantemant = Math.floor(Math.random() * (4 - 1)) + 1;
    }

    get enchantemant() {
        return this.enchantemant;
    }
}

module.exports = Enchanteur;
