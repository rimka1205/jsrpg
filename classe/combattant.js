const Joueur = require("./joueur");

class Combattant extends Joueur {
    constructor() {
        super();
        super.strength += 5;
    }

    attack(name) {
        if (name == "normal") {
            return super.strength;
        }
    }
}

module.exports = Combattant
