class Joueur {
    pv;
    strength;
    intelligence;

    constructor() {
        this.pv = 0;
        this.strength = 0;
        this.intelligence = 0;
    }

    get pv() {
        return this.pv;
    }

    get strength() {
        return this.strength;
    }

    get intelligence() {
        return this.intelligence;
    }

    set pv(pv) {
        this.pv = pv;
    }

    set strength(strength) {
        this.strength = strength;
    }

    set intelligence(intelligence) {
        this.intelligence = intelligence;
    }
}

module.exports = Joueur;
