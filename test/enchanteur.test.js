const Enchanteur = require("../classe/enchanteur");
let Ench = new Enchanteur;

test('Creer un Enchanteur', () => {
    expect(Ench.pv).toBe(0);
    expect(Ench.strength).toBe(0);
    expect(Ench.intelligence).toBe(5);
    expect(Ench.enchantemant).toBeLessThan(4);
    expect(Ench.enchantemant).toBeGreaterThan(0);
})

test('Attaque normale', () => {
    expect(Ench.attack('normal')).toBe(5);
})


/**test('Attaque slash', () => {
    expect(Enchanteur.attack('slash')).toBe(10)
})**/
