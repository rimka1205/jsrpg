const Chevalier = require("../classe/chevalier");
let Chev = new Chevalier;

test('Creer un chevalier', () => {
    expect(Chev.pv).toBe(5);
    expect(Chev.strength).toBe(10);
    expect(Chev.intelligence).toBe(0);
})

test('Attaque normale', () => {
    expect(Chev.attack('normal')).toBe(10);
})

/**test('Attaque slash', () => {
    expect(Chev.attack('slash')).toBe(15)
})**/
