const Barbare = require("../classe/barbare");
let Bar = new Barbare;

test('Creer un barbare', () => {
    expect(Bar.pv).toBe(10);
    expect(Bar.strength).toBe(5);
    expect(Bar.intelligence).toBe(0);
})

test('Attaque normale', () => {
    expect(Bar.attack('normal')).toBe(5);
})

/**test('Attaque slash', () => {
    expect(Barbare.attack('slash')).toBe(10)
})**/
