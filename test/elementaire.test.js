const Elementaire = require("../classe/elementaire");
let Elem = new Elementaire;

test('Creer un Elementaire', () => {
    expect(Elem.pv).toBe(0);
    expect(Elem.strength).toBe(0);
    expect(Elem.intelligence).toBe(5);
    tableElement = ['eau','feu','air','terre'];
    elementTest = tableElement.includes(Elem.element);
    expect(elementTest).toBeTruthy();
})

test('Attaque normale', () => {
    expect(Elem.attack('normal')).toBe(5);
})

/**test('Attaque slash', () => {
    expect(Elementaire.attack('slash')).toBe(10)
})**/
